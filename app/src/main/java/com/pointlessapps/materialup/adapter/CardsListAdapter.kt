package com.pointlessapps.materialup.adapter

import android.content.Context
import android.content.DialogInterface
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pointlessapps.materialup.R
import com.pointlessapps.materialup.models.Card
import com.pointlessapps.materialup.utils.Utils
import com.squareup.picasso.Picasso

class CardsListAdapter(private var cardsList: MutableList<Card>, private var small: Boolean = false) : RecyclerView.Adapter<CardsListAdapter.DataObjectHolder>() {

	private var context: Context? = null

	private var clickListener: ((pos: Int) -> Unit)? = null

	inner class DataObjectHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
		val title: AppCompatTextView = itemView.findViewById(R.id.title)
		val img: AppCompatImageView? = itemView.findViewById(R.id.img)
		val voteCount: AppCompatTextView? = itemView.findViewById(R.id.voteCount)

		init {
			itemView.findViewById<View>(R.id.bg)?.setOnClickListener {clickListener?.invoke(adapterPosition)}
			if(small) {
				itemView.findViewById<View>(R.id.bg)?.layoutParams?.height = Utils.dpToPx(context!!, 150)
				itemView.findViewById<View>(R.id.bg)?.layoutParams?.width = Utils.dpToPx(context!!, 150)
			}
		}
	}

	init {
		setHasStableIds(true)
	}

	fun setClickListener(clickListener: (pos: Int) -> Unit) {
		this.clickListener = clickListener
	}

	override fun getItemId(position: Int): Long {
		return cardsList[position].hashCode().toLong()
	}

	override fun getItemViewType(position: Int): Int {
		return if(cardsList[position].isHeader()) HEADER else ITEM
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataObjectHolder {
		context = parent.context
		return DataObjectHolder(LayoutInflater.from(parent.context).inflate(if(viewType == ITEM) R.layout.card_list_item_view else R.layout.header_list_item_view, parent, false))
	}

	override fun onBindViewHolder(holder: DataObjectHolder, position: Int) {
		holder.title.text = cardsList[position].title
		holder.voteCount?.text = cardsList[position].voteCount.toString()
		if(holder.img != null)
			Picasso.get().load(cardsList[position].imgUrl).into(holder.img)
	}

	override fun getItemCount(): Int {
		return cardsList.size
	}

	companion object {
		const val HEADER = 0
		const val ITEM = 1
	}
}