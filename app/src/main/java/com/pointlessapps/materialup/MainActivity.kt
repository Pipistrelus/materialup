package com.pointlessapps.materialup

import android.os.Bundle
import android.support.transition.AutoTransition
import android.support.transition.TransitionManager
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.AppCompatEditText
import android.support.v7.widget.CardView
import android.view.KeyEvent
import android.view.View
import android.view.ViewAnimationUtils
import com.pointlessapps.materialup.fragments.BaseFragment
import com.pointlessapps.materialup.fragments.FeedFragment
import com.pointlessapps.materialup.fragments.SearchFragment
import com.pointlessapps.materialup.scraper.WebScraper
import com.pointlessapps.materialup.utils.Utils

class MainActivity : FragmentActivity() {

	companion object {
		const val BASE_URL = "https://www.uplabs.com/android/"
		const val SEARCH_URL = "https://www.uplabs.com/search?q="
	}

	private lateinit var webScraper: WebScraper

	private val fragments = arrayOfNulls<BaseFragment>(2)
	private var currentFragment: BaseFragment? = null

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		setWebScraper()
		setFragments()
		setSearchView()

		switchFragment(fragments[0]!!)
	}

	private fun setWebScraper() {
		webScraper = WebScraper(this)
		webScraper.loadURL(BASE_URL)
	}

	private fun setFragments() {
		fragments[0] = FeedFragment().setGetHtmlListener { url ->
			webScraper.loadURL(url ?: BASE_URL)
			webScraper.scrollPage()
		}

		fragments[1] = SearchFragment().setGetHtmlListener { url ->
			if(url != null) webScraper.loadURL(url)
			else webScraper.reload()
			webScraper.scrollPage()
		}

		webScraper.setOnPageLoadedListener {
			if(currentFragment != null)
				webScraper.getHtml {
					currentFragment?.setHtml(it)
				}
		}
	}

	private fun switchFragment(fragment: BaseFragment) {
		val fragmentTransaction = supportFragmentManager.beginTransaction()
		fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
		if(currentFragment != null)
			fragmentTransaction.replace(R.id.container, fragment).commit()
		else fragmentTransaction.add(R.id.container, fragment).commit()
		currentFragment = fragment
	}

	private fun setSearchView() {
		val searchView = findViewById<AppCompatEditText>(R.id.searchView)
		searchView?.setImeActionLabel(resources.getString(R.string.search), KeyEvent.KEYCODE_ENTER)
		searchView?.setOnKeyListener { v, keyCode, _ ->
			if(keyCode == KeyEvent.KEYCODE_ENTER && currentFragment == fragments[1]) {
				val query = (v as AppCompatEditText).text?.toString()
				webScraper.loadURL("$SEARCH_URL$query")
				(fragments[1] as SearchFragment).startedSearching()
			}
			return@setOnKeyListener true
		}
	}

	fun clickSearch(v: View) {
		val searchContainer = findViewById<CardView>(R.id.searchContainer)
		searchContainer?.alpha = 1F
		searchContainer?.visibility = View.VISIBLE
		ViewAnimationUtils.createCircularReveal(
			searchContainer,
			v.x.toInt() + v.width / 2,
			v.y.toInt() + v.height / 2,
			0F,
			Utils.windowSize(applicationContext).x.toFloat()
		)
			.setDuration(300)
			.start()

		switchFragment(fragments[1]!!)
	}

	fun clickBack(v: View?) {
		val searchContainer = findViewById<CardView>(R.id.searchContainer)
		searchContainer?.animate()?.alpha(0F)?.withEndAction {
			searchContainer.visibility = View.GONE
		}?.start()

		TransitionManager.beginDelayedTransition(findViewById(R.id.topBarContainer)!!, AutoTransition())

		switchFragment(fragments[0]!!)
	}

	override fun onBackPressed() {
		if(currentFragment == fragments[1])
			clickBack(null)
		else super.onBackPressed()
	}
}
