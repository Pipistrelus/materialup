package com.pointlessapps.materialup.utils

import android.content.Context
import android.graphics.Point

object Utils {

	fun dpToPx(context: Context, dp: Int) : Int {
		return (context.resources.displayMetrics.density * dp).toInt()
	}

	fun windowSize(context: Context): Point {
		return Point(context.resources.displayMetrics.widthPixels, context.resources.displayMetrics.heightPixels)
	}
}
