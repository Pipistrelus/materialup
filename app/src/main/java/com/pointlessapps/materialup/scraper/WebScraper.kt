package com.pointlessapps.materialup.scraper

import android.annotation.SuppressLint
import android.content.Context
import android.os.AsyncTask
import android.os.Build
import android.os.Handler
import android.util.Log
import android.view.View
import android.webkit.JavascriptInterface
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient

@SuppressLint("SetJavaScriptEnabled,unused")
class WebScraper(context: Context) {

	@Volatile private var htmlBool: Boolean = false
	private var onPageLoadedListener: (() -> Unit)? = null
	private val web: WebView = WebView(context)
	private val handler: Handler = Handler()

	fun getHtml(callback: (String?) -> Unit) {
		web.evaluateJavascript("(function() { return ('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>'); })();") {
			val output = it.replace("\\u003C", "<").replace("\\\"", "\"")
			callback.invoke(output.substring(1, output.lastIndex - 1))
		}
	}

	init {
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) WebView.enableSlowWholeDocumentDraw()
		web.settings.javaScriptEnabled = true
		web.settings.blockNetworkImage = true
		web.settings.loadsImagesAutomatically = false
		web.webChromeClient = object : WebChromeClient() {
			override fun onProgressChanged(view: WebView?, newProgress: Int) {
				if(newProgress == 100)
					onPageLoadedListener?.invoke()
				super.onProgressChanged(view, newProgress)
			}
		}
		web.addJavascriptInterface(JSInterface(), "HtmlViewer")
		web.webViewClient = object : WebViewClient() {
			override fun onPageFinished(view: WebView, url: String) {
				web.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))
				web.layout(0, 0, web.measuredWidth, web.measuredHeight)
				web.isDrawingCacheEnabled = true
			}
		}
	}

	fun scrollPage() {
		htmlBool = true
		web.evaluateJavascript("javascript:window.scrollTo(0, document.body.scrollHeight);window.HtmlViewer.scrolled();") { htmlBool = false }
		handler.postDelayed({ htmlBool = false }, 5)
		while(htmlBool) {
		}
	}

	fun loadURL(URL: String) {
		web.loadUrl(URL)
	}

	fun reload() {
		web.loadUrl(web.url)
	}

	fun setOnPageLoadedListener(onPageLoadedListener: () -> Unit) {
		this.onPageLoadedListener = onPageLoadedListener
	}

	private inner class JSInterface {
		@JavascriptInterface
		fun scrolled() {
			htmlBool = false
			onPageLoadedListener?.invoke()
		}
	}
}