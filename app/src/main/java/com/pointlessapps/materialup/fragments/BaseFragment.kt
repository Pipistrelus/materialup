package com.pointlessapps.materialup.fragments

import android.support.v4.app.Fragment

abstract class BaseFragment : Fragment() {

	protected var getHtmlListener: ((String?) -> Unit)? = null

	abstract fun setHtml(html: String?)

	fun setGetHtmlListener(getHtmlListener: (url: String?) -> Unit): BaseFragment {
		this.getHtmlListener = getHtmlListener
		return this
	}
}