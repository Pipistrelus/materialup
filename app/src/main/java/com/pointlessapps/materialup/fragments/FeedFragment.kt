package com.pointlessapps.materialup.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.pointlessapps.materialup.R
import com.pointlessapps.materialup.adapter.CardsListAdapter
import com.pointlessapps.materialup.models.Card
import com.pointlessapps.materialup.models.Row
import org.jsoup.Jsoup
import org.jsoup.nodes.Element

class FeedFragment : BaseFragment() {

	private var rootView: ViewGroup? = null

	private lateinit var cardsListAdapter: CardsListAdapter
	private lateinit var cardsList: RecyclerView

	private var currentCards = mutableListOf<Card>()

	private var loadedFrom = 0
	private var loading = false

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		if(rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_card_list, container, false) as ViewGroup?

			setCardsList()
			getHtmlListener?.invoke(null)

			rootView?.findViewById<SwipeRefreshLayout>(R.id.swipeLayout)?.setOnRefreshListener {
				loading = true
				getHtmlListener?.invoke(null)
			}

			rootView?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.VISIBLE
		}
		return rootView
	}

	private fun setCardsList() {
		cardsListAdapter = CardsListAdapter(currentCards)
		cardsListAdapter.setClickListener { }//openCard(currentCards[pos])}

		cardsList = rootView!!.findViewById(R.id.cardsList)
		cardsList.adapter = cardsListAdapter
		cardsList.layoutManager = LinearLayoutManager(rootView!!.context, LinearLayoutManager.VERTICAL, false)
		cardsList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
			override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
				super.onScrolled(recyclerView, dx, dy)
				val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager?
				if(!loading && linearLayoutManager!!.itemCount <= linearLayoutManager.findLastVisibleItemPosition() + 2) {
					loading = true
					getHtmlListener?.invoke(null)
				}
			}
		})
	}

	private fun updateCardsList(cards: MutableList<Card>) {
		currentCards.addAll(cards)
		cardsListAdapter.notifyDataSetChanged()
	}

	private fun getCards(baseElement: Element): MutableList<Card> {
		val cards = mutableListOf<Card>()
		val rows = baseElement.getElementsByClass("row")
		for(row in rows) {
			for(child in row.children()) {
				val element = child.getElementsByClass("card__title")
				val title = element.text()
				val siteUrl = element.attr("href")
				val imgUrl = child.getElementsByClass("preview")[0].attr("src")
				val text = child.getElementsByClass("count").text()
				var voteCount = 0
				if(!text.isBlank()) voteCount = text.toInt()
				cards.add(Card(title, imgUrl, siteUrl, voteCount))
			}
		}
		return cards
	}

	private fun loadCards(html: String?, callback: (() -> Unit)? = null) {
		if(html == null) return

		val rows = mutableListOf<Row>()
		val doc = Jsoup.parse(html)
		while(true) {
			val el = doc.getElementById("js-showcase-$loadedFrom") ?: break
			val cards = getCards(el)

			rows.add(Row(el.getElementsByTag("h3")[0].text(), cards))
			loadedFrom++
		}

		for(row in rows) {
			updateCardsList(mutableListOf(Card(row.title, null, null, null)))
			updateCardsList(row.cards)
		}

		callback?.invoke()
	}

	override fun setHtml(html: String?) {
		loadCards(html) {
			loading = false
			rootView?.findViewById<SwipeRefreshLayout>(R.id.swipeLayout)?.isRefreshing = false
			rootView?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE
		}
	}
}