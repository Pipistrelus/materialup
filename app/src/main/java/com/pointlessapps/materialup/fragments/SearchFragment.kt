package com.pointlessapps.materialup.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.pointlessapps.materialup.R
import com.pointlessapps.materialup.adapter.CardsListAdapter
import com.pointlessapps.materialup.models.Card
import org.jsoup.Jsoup

class SearchFragment : BaseFragment() {

	private var rootView: ViewGroup? = null

	private lateinit var cardsListAdapter: CardsListAdapter
	private lateinit var cardsList: RecyclerView

	private var currentCards = mutableListOf<Card>()

	private var loading = false
	private var scrollY = 0

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		if(rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_card_list, container, false) as ViewGroup?

			setCardsList()

			rootView?.findViewById<SwipeRefreshLayout>(R.id.swipeLayout)?.setOnRefreshListener {
				loading = true
				getHtmlListener?.invoke(null)
			}
		}
		return rootView
	}

	private fun setCardsList() {
		cardsListAdapter = CardsListAdapter(currentCards)
		cardsListAdapter.setClickListener { }//openCard(currentCards[pos])}

		cardsList = rootView!!.findViewById(R.id.cardsList)
		cardsList.adapter = cardsListAdapter
		cardsList.layoutManager = LinearLayoutManager(rootView!!.context, LinearLayoutManager.VERTICAL, false)
		cardsList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
			override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
				super.onScrolled(recyclerView, dx, dy)
				val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager?
				if(!loading && linearLayoutManager!!.itemCount <= linearLayoutManager.findLastVisibleItemPosition() + 2) {
					loading = true
					scrollY = linearLayoutManager.findLastVisibleItemPosition()
					Log.d("LOG!", "scrollY: $scrollY")
					getHtmlListener?.invoke(null)
				}
			}
		})
	}

	private fun performSearch(html: String?, callback: (() -> Unit)? = null) {
		if(html == null) return

		val doc = Jsoup.parse(html)
		val el = doc.getElementsByClass("content")

		if(el.isNullOrEmpty()) return

		val cards = mutableListOf<Card>()
		val row = ((el[0] ?: return).getElementsByClass("row") ?: return)[0]
		for(child in row.children()) {
			try {
				val element = child.getElementsByClass("card__title")
				val title = element.text()
				val siteUrl = element.attr("href")
				val imgUrl = child.getElementsByClass("preview")[0].attr("src")
				val textVoteCount = child.getElementsByClass("count").text()
				var voteCount = 0
				if(!textVoteCount.isBlank()) voteCount = textVoteCount.toInt()
				cards.add(Card(title, imgUrl, siteUrl, voteCount))
			} catch(e: Exception) {
				e.printStackTrace()
			}
		}

		currentCards.clear()
		updateCardsList(mutableListOf(Card("Search Result", null, null, null)))
		updateCardsList(cards)

		callback?.invoke()
	}

	private fun updateCardsList(cards: MutableList<Card>) {
		currentCards.addAll(cards)
		cardsListAdapter.notifyDataSetChanged()
	}

	override fun setHtml(html: String?) {
		performSearch(html) {
			loading = false
			if(scrollY >= 0) rootView?.findViewById<RecyclerView>(R.id.cardsList)?.scrollToPosition(scrollY)
			rootView?.findViewById<SwipeRefreshLayout>(R.id.swipeLayout)?.isRefreshing = false
			rootView?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.GONE
		}
	}

	fun startedSearching() {
		rootView?.findViewById<ProgressBar>(R.id.progressBar)?.visibility = View.VISIBLE
		currentCards.clear()
		cardsListAdapter.notifyDataSetChanged()
		scrollY = 0
	}
}
