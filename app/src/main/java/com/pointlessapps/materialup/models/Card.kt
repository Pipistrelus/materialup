package com.pointlessapps.materialup.models

class Card(var title: String, var imgUrl: String?, var siteUrl: String?, var voteCount: Int?) {

	fun isHeader() : Boolean {
		return imgUrl == null || siteUrl == null || voteCount == null
	}
}