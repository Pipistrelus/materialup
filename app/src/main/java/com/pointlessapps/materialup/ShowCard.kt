package com.pointlessapps.materialup

import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.text.Html
import com.pointlessapps.materialup.scraper.WebScraper
import com.squareup.picasso.Picasso
import org.jsoup.Jsoup

class ShowCard : Activity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_show)

		val url = intent.getStringExtra("url")
		val webScraper = WebScraper(this)
		val image = findViewById<AppCompatImageView>(R.id.image)
		val text = findViewById<AppCompatTextView>(R.id.text)

		webScraper.loadURL(url)

		webScraper.setOnPageLoadedListener {
			webScraper.getHtml {
				val doc = Jsoup.parse(it)
				Picasso.get().load(doc.getElementsByClass("preview")[0].attr("src")).into(image)
				if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) text.text = Html.fromHtml(doc.getElementsByClass("post__side-description")[0].text(), Html.FROM_HTML_MODE_LEGACY)
				else text.text = Html.fromHtml(doc.getElementsByClass("post__side-description")[0].text())
			}
		}
	}
}